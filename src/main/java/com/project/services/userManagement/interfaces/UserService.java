package com.project.services.userManagement.interfaces;

import java.util.List;

import com.project.models.userManagement.Role;
import com.project.models.userManagement.User;

public interface UserService {
    /* USER */
    User saveUser(User user);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User>getUsers();

    /* ROLE */
    Role saveRole(Role role);
    Role getRole(String roleName);
    List<Role>getRoles();

}
