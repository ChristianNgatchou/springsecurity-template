package com.project.repository.userManagement;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.models.userManagement.User;

public interface UserRepository extends JpaRepository<User, Long>{
    User findByUsername(String username);   
}