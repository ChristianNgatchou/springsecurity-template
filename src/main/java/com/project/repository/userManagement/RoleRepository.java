package com.project.repository.userManagement;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.models.userManagement.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
    Role findByname(String roleName);   
}
